function varargout = Diffusion_GUI(varargin)
% DIFFUSION_GUI MATLAB code for Diffusion_GUI.fig
%      DIFFUSION_GUI, by itself, creates a new DIFFUSION_GUI or raises the existing
%      singleton*.
%
%      H = DIFFUSION_GUI returns the handle to a new DIFFUSION_GUI or the handle to
%      the existing singleton*.
%
%      DIFFUSION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIFFUSION_GUI.M with the given input arguments.
%
%      DIFFUSION_GUI('Property','Value',...) creates a new DIFFUSION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Diffusion_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Diffusion_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Diffusion_GUI

% Last Modified by GUIDE v2.5 24-Feb-2017 19:17:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Diffusion_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @Diffusion_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Diffusion_GUI is made visible.
function Diffusion_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Diffusion_GUI (see VARARGIN)
handles.peak = peaks(35);
handles.membrane = membrane;
[x, y] = meshgrid(-8:0.5:8);
r = sqrt(x.^2 + y.^2) + eps;
sinc = sin(r) ./ r;
handles.sinc = sinc;
handles.current_data = handles.peak;
surf(handles.current_data);

% Choose default command line output for Diffusion_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Diffusion_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Diffusion_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in plot_type.
function plot_type_Callback(hObject, eventdata, handles)
% hObject    handle to plot_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(hObject, 'Value');
str = get(hObject, 'String');
switch str{val}
    case 'peaks'
        handles.current_data = handles.peak;
    case 'membrane'
        handles.current_data = handles.membrane;
    case 'sinc'
        handles.current_data = handles.sinc;
end
guidata(hObject, handles);
        

% Hints: contents = cellstr(get(hObject,'String')) returns plot_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_type


% --- Executes during object creation, after setting all properties.
function plot_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in surf.
function surf_Callback(hObject, eventdata, handles)
% hObject    handle to surf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
surf(handles.current_data);


% --- Executes on button press in mesh.
function mesh_Callback(hObject, eventdata, handles)
% hObject    handle to mesh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mesh(handles.current_data);


% --- Executes on button press in contour.
function contour_Callback(hObject, eventdata, handles)
% hObject    handle to contour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contour(handles.current_data);


% --- Executes during object creation, after setting all properties.
function diff_properties_CreateFcn(hObject, eventdata, handles)
% hObject    handle to diff_properties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
data = {10,'m/s';
        0 ,'ppm';
        1 ,'ppm';
        1 ,'s';
        10, '-';
        1 , 'm';
        2 , '-'};
set(hObject, 'Data', data);

handle_data.Dc                    = data{1,1};
handle_data.initial_concentration = data{2,1};
handle_data.constant_source       = data{3,1};
handle_data.time                  = data{4,1};
handle_data.nn                    = data{5,1};
handle_data.length                = data{6,1};
handle_data.nqp                   = data{7,1};

handles.diff_properties = handle_data;

guidata(hObject, handles);




% --- Executes during object deletion, before destroying properties.
function diff_properties_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to diff_properties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function diff_properties_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to diff_properties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in diff_properties.
function diff_properties_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to diff_properties (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
data = get(hObject, 'Data');
handle_data.Dc                    = data{1,1};
handle_data.initial_concentration = data{2,1};
handle_data.constant_source       = data{3,1};
handle_data.time                  = data{4,1};
handle_data.nn                    = data{5,1};
handle_data.length                = data{6,1};
handle_data.nqp                   = data{7,1};

handles.diff_properties = handle_data;
display('Input values updated!');
guidata(hObject, handles);





% --- Executes when selected cell(s) is changed in diff_properties.
function diff_properties_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to diff_properties (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in solve.
function solve_Callback(hObject, eventdata, handles)
% hObject    handle to solve (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Pass in the input values to solve diffusion equation
diffusion(handles);
