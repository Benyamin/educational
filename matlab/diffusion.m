function diffusion(varargin)
    handles = varargin{1};
    data = handles.diff_properties;
    % Construct mesh
    % Number of nodes
    N = data.nn;
    L = data.length;
    m     = mesh(N, L);
    % Construct integration scheme (rule=1/2)
    rule = data.nqp;
    q    = quadrature(rule);
    
    % Construct lumped mass matrix.
    M = zeros(N, 1);
    for e = 1:m.Ne
        Me  = zeros(2,2);
        conn = m.connectivity(:, e);
        xe = m.X(conn);
        for i = 1:q.nq
            Nq  = shape(q.xi(i));            
            J  = grad_shape(q.xi(i)) * xe;
            Me = Me + Nq'*Nq * det(J)*q.w;
        end
        M(conn) = M(conn) + sum(Me,2);
    end
    invM = M.^-1;
    % Diffusion coefficient
    Dc = data.Dc;

    total_time = data.time;
    dt = 0.001;
    num_steps = floor(total_time / dt);
    
    C_old = zeros(N, 1);
    
    C_old(1) = data.initial_concentration;
    
    % Global external flux vector declaration
    Fext = zeros(N, 1);
    Cs0 = data.constant_source;
    Cs = Cs0*ones(N,1);
    C = C_old;

    % Time loop
    for step = 1:num_steps
        K    = zeros(N, N);  
        % Loop over elements
        for e = 1:m.Ne
            conn = m.connectivity(:, e);
            % Element coordinate
            xe   = m.X(conn);
            Ke = zeros(2, 2);
            % Loop over integration points
            for i = 1:q.nq

                % Shape function at integration point
                J = grad_shape(q.xi(i)) * xe;
                dN = grad_shape(q.xi(i))/J;
                Ke = Ke + Dc * (dN')*dN*det(J)*q.w;
            end
            %Scatter element forces to global external force vector
            K(conn, conn) = K(conn, conn) + Ke;
        end
        C_new = (eye(N) - dt * diag(invM) * K) * C_old + dt * diag(invM)*Fext;
        C_new(1) =  Cs0;
        C = [C , C_new];
        C_old = C_new;
        
    end
    surf(linspace(0, total_time, num_steps+1), linspace(0,L,N), C,...
        'EdgeColor','none'),
    % Create zlabel
    zlabel({'Concentration (ppm)'},'FontSize',11);
    
    % Create ylabel
    ylabel({'Length (mm)'},'FontSize',11);
    
    % Create xlabel
    xlabel({'Time (s)'},'FontSize',11);
    shg;
end

% Function to construct mesh 
function [mesh] = mesh(N, L)
    % Coordinate of nodes
    mesh.X = linspace(0, L, N)';
    % Number of elements
    mesh.Ne = N - 1;
    % Element size
    mesh.h  = L/mesh.Ne;
    % Construct connectivity matrix
    mesh.connectivity = zeros(2, mesh.Ne);
    for i = 1:mesh.Ne
        mesh.connectivity(:,i) = [i; i+1];
    end
end

% Quadrature
function [q] = quadrature(rule)
    % Set integration scheme
    % Two-point rule
    if rule==2
        q.xi = [-1/sqrt(3);
                 1/sqrt(3)];
        q.w  = 1;
    % One-point rule
    elseif rule==1
        q.xi = 0;
        q.w  = 2;
    else
        error('Wrong quadrature rule!')
    end
    % Number of Gauss points
    q.nq = length(q.xi);
end

% Shape function
function [N] = shape(xi)
    N = [0.5*(1-xi), 0.5*(1+xi)];
end

% Gradient of shape function
function [dNdp] = grad_shape(xi)
    dNdp = [-0.5, 0.5];
end