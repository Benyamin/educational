### What is this repository for? ###

* This repository is meant for educational purposes and includes simple Matlab codes that might come handy in numerical analysis.
* version 1.0

### How do I get set up? ###

* Download the files and run with a proper software.

### Contribution guidelines ###

* You can request to access to the repository to edit/add codes.

### Who do I talk to? ###

* Author: Benyamin Gholami Bazehhour
* Email: bgholam1@asu.edu
